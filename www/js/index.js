$( document ).ready(function() {
    // Remove sombreamento e formato circular da figura de marca do dashboard
    $(".brand-image").toggleClass("img-circle elevation-3")

    // Remove a borda dos botões
    $(".btn-default").css("border-style","none")

    // Remove a borda dos botões
    //$(".main-header").toggleClass("bg-primary")

    // Remove a borda dos botões
    $(".my-card").toggleClass("card")

    $(".card.mx-sm-1.p-3").toggleClass("p-5")
    $(".text-primary.text-center.mt-3").css("margin-top", "7rem")
    $(".text-secondary.text-center.mt-3").css("margin-top", "7rem")
    $(".text-primary.text-center.mt-3").toggleClass("mt-3")
    $(".text-secondary.text-center.mt-3").toggleClass("mt-3")

    //$("border-primary.shadow.text-primary.p-3.my-card").toggleClass("shadow")
});


