box::use(
  dplyr[group_by, ungroup, mutate, select, filter],
  bs4Dash,
  echarts4r[
    e_charts,
    e_title,
    e_line,
    echarts4rOutput,
    e_tooltip,
    renderEcharts4r,
    e_text_style,
    e_labels,
    e_common,
    e_y_axis,
  ],
  leaflet,
  magrittr[`%>%`],
  shiny[
    fluidPage,
    moduleServer,
    NS,
    tagList,
    tags,
    fluidRow,
    column,
    icon,
    observe,
    ],
  readxl,
  stats,
  shinybusy,
  summaryBox[summaryBox3],
  shinycssloaders[withSpinner],
  )

summaryBoxImage <- function(title, value, icon, cor="#ffffff", width = 4, style = "info") {

  infotag  <- tags$div(
    class = paste0("col-md-", width),
    tags$div(
      class = paste0("card border-", style, " mx-sm-1 p-3"),
      tags$div(
        class = paste0("card icon-box border-", style, " shadow text-", style , " p-3"),
        style = sprintf("background: %s", cor),
        tags$span(
          class = "meu-icone",
          `aria-hidden` = "true",
          style = sprintf("background: %s", cor),
          tags$img(src = icon, style="max-width: 100%")
        )
      ),
      tags$div(
        class = paste0("text-", style, " text-center mt-3"),
        tags$h4(title)
      ),
      tags$div(
        class = paste0("text-", style, " text-center mt-2"),
        tags$h1(value)
      )
    )
  )

  #htmltools::htmlDependencies(infotag) <- loadFontAwesome()
  #htmltools::browsable(infotag)
}

#' @export
ui <- function(id) {  
  ns <- NS(id)

  ## e_common(
  ##   font_family = "Georama"
  ## )

  largura_trail <- 15
  fluidPage(
    fluidRow(
      summaryBoxImage("Renda gerada para associações", "R$75.712", width=4, icon="img/reciclo.png", style="orange", cor="#ef7a2a"),
      summaryBox3("Ecopontos", "90", width=4, icon="fas fa-recycle", style="primary"),
      summaryBox3("Lixeiras Subterrâneas", "19", width=4, icon = "fas fa-dumpster", style="secondary")
    ),
    fluidRow(
      column(
        8,
        style = "height: 100%;",
        bs4Dash$box(
          title = "Volume de resíduos destinados ao aterro (Novo ASMOC)",
          width = 12,
          solidHeader = TRUE,
          status = "primary",
          withSpinner(            
            echarts4rOutput(ns("volume_residuo_aterro"))
          ) 

        )
      ),
      column(
        4,
        bs4Dash$box(
          title = "Taxa de Reciclagem",
          width = 12,
          solidHeader = TRUE,
          status = "primary",
          shinybusy$progress_semicircle(
            value = 0,
            color = "#112446",
            stroke_width = largura_trail,
            easing = "easeInOut",
            duration = 1400,
            trail_color = "#eee",
            trail_width = largura_trail,
            text = "auto",
            text_color = "#000",
            width = "90%",
            height = "100%",
            shiny_id = ns("meta_reciclagem")
          )
        )
      )
    )
  )
  ## fluidPage(
  ##   fluidRow(
  ##     summaryBoxImage("Renda gerada para associações", "R$75.712", width=4, icon="img/reciclo.png", style="orange"),
  ##     summaryBox3("Ecopontos", "90", width=4, icon="fas fa-recycle", style="primary"),
  ##     summaryBox3("Lixeiras Subterrâneas", "19", width=4, icon = "fas fa-dumpster", style="secondary")
  ##   ),
  ##   fluidRow(
  ##     bs4Dash$box(
  ##       title = "Volume de resíduos destinados ao aterro (Novo ASMOC)",
  ##       width = 8,
  ##       solidHeader = TRUE,
  ##       status = "primary",
  ##       echarts4rOutput(ns("volume_residuo_aterro"))
  ##     ),
  ##     bs4Dash$box(
  ##       title = "Taxa de Reciclagem",
  ##       width = 4,
  ##       solidHeader = TRUE,
  ##       status = "primary",
  ##       shinybusy$progress_semicircle(
  ##         value = 0,
  ##         color = "#112446",
  ##         stroke_width = largura_trail,
  ##         easing = "easeInOut",
  ##         duration = 1400,
  ##         trail_color = "#eee",
  ##         trail_width = largura_trail,
  ##         text = "auto",
  ##         text_color = "#000",
  ##         width = "90%",
  ##         height = "100%",
  ##         shiny_id = ns("meta_reciclagem")
  ##       )
  ##     )                 
  ##   )
  ## )

  
}

#' @export
server <- function(id) {
  moduleServer(id, function(input, output, session) {

    print("aksjdhaksdjhsa ")

    ## tipo_coleta <- data.frame(
    ##   dia = c(),
    ##   domiciliar = c(),
    ##   entulho = c(),
    ##   podacao = c(),
    ##   ponto_lixo = c()
    ## )

    aterro <- readxl$read_excel("app/data/OADisplay.xls", skip=2)
    ##browser()

    observe({
      shinybusy$update_progress("meta_reciclagem", 0.09)
    })
        
    output$volume_residuo_aterro <- renderEcharts4r({
      aterro |> 
        group_by(`DATA ENTRADA`) |>
        mutate(peso_total = sum(`PESO BRUTO`)) |>
        ungroup() |>
        group_by(`MATERIAL`) |>
        e_charts(`DATA ENTRADA`) |> 
        e_line(peso_total) |>
        e_text_style(
          fontSize = 18
        ) |>
        e_y_axis(
          fontSize = 46
        ) |>
        e_tooltip()
    })

    ## output$indicador2 <- renderEcharts4r({
    ##   datasets::iris |> 
    ##     group_by(Species) |> 
    ##     e_charts(Sepal.Length) |> 
    ##     e_line(Sepal.Width) |> 
    ##     e_title("Grouped data")
    ## })

    ## output$indicador3 <- renderEcharts4r({
    ##   datasets::iris |> 
    ##     group_by(Species) |> 
    ##     e_charts(Sepal.Length) |> 
    ##     e_line(Sepal.Width) |> 
    ##     e_title("Grouped data")
    ## })
    

  })    
}


